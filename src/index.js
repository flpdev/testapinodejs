const express = require('express');
const app = express();
let telegram = require('./routes/telegram')


// Settings
app.set('port', 3000 | process.env.PORT);

// Middlewares
app.use(express.json());
telegram.telegram();

// Routes
app.use('/employee' ,require('./routes/employees'))
let router = require('./routers/upload.router.js');
app.use('/files', router);

// Starting the server
app.listen(app.get('port'),()=>{
    console.log('Server on port' , app.get('port'));
});

