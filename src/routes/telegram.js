exports.telegram = function(){
    const TelegramBot = require('node-telegram-bot-api'); 
    const mysqlConnection = require('../database');
    const Player = require('../models/player')
    const token = '906252370:AAGWphJnqCb23Oj5aGBTIyqWD6nlByrfAO0';
    const bot = new TelegramBot(token, {polling:true});
    const request = require('request');


    //TEsT
    var players = []
    var p1 = new Player.create('player1', 2);
    var p2 = new Player.create('player2', 4);
    players.push(p1,p2);
    players.forEach(player => {
        if(player.name === 'ferran'){
            player.wordEntered = 10
        }
    })
    console.log(players)

    //END TEST


    var commands = [
        {name: '/start', desc: 'starts the bot'},
        {name: '/newgame <numberPlayers>,<numberTeams>,<wordsPerPlayer>,<%randomWords>', desc: 'Creates a new game instance with passed config'},
        {name: '/word <word>', desc: 'adds a word to the word pool'},
        {name: '/addme', desc: 'adds the player to the game'},
        {name: '/reset', desc: 'Resets the current game instance'},
        {name: '/help', desc: 'duh'}
     ]

    var numberOfWords = 0;
    var wordEntered = 0;

    var currentGame = ({
        numPlayers: 0,
        numTeams: 0,
        wordsPerPlayer: 0,
        random_percentatge: 0,
        wordsToInput: 0, 
        WordsPerPlayerToInput: 0   
    })




    //BOT

    bot.on('message', function(msg){
        var chatID = msg.chat.id;
        var text = msg.text.toString().toLowerCase();
        var msgID = msg.message_id;
        
        if(text.includes('/start')){
            bot.deleteMessage(chatID, msgID);
            bot.sendMessage(msg.chat.id, "starting...")
        }
       
        else if(text.includes('/word')){
            if(currentGame.wordsToInput>0){
                if(wordEntered<currentGame.wordsToInput){
                    var word = text.split(' ')[1];
                    var sender = msg.from.first_name;
                    values = [1, word, sender]
                    mysqlConnection.query('INSERT INTO WORD VALUES (NULL, ?,?,?)', values, (err, rows, fields) => {
                        if(err){
                            console.log(err.message, 'hola')
                            bot.deleteMessage(chatID, msgID);
                            bot.sendMessage(msg.chat.id, "error...")
                        }else{
                            console.log(msg)
                            console.log('borrant', chatID, msgID)
                            bot.deleteMessage(chatID, msgID);
                            //change next line
                            bot.sendMessage(msg.chat.id, sender + " has inserted " + wordEntered + " words;")
                        }
                    })
                }else{
                    console.log("all words saved")
                    bot.sendMessage(msg.chat.id, "All words saved successfully :)")
                }
                
            }else{
                bot.sendMessage(msg.chat.id, "First set a number of words with command: /setwords n");
            }
            
        }
        else if(text.includes('/addme')){
            addPlayer(msg);
        }
        else if(text.includes('/reset')){
            reset(msg);
        } else if(text.includes('/newgame')){
            newGame(msg);
        } else if(text.includes('/help')){
            help(msg);
        } 

        
    }) 


    bot.on("polling_error", function(error){
        console.log(error)
    })


    var addPlayer = function(msg){
        var canAddPlayer = true;
        var sender = msg.from.first_name.toString().toLowerCase();
        var p = new Player.create(sender, 0); 

        players.forEach(player => {
            if(player.name === p.name){
                canAddPlayer =  false;
            }
        }); 

        if(canAddPlayer){
            players.push(p);
        bot.sendMessage(msg.chat.id, "Player " + p.name + " added to the game!");
        }else{
            bot.sendMessage(msg.chat.id, "Player " + p.name + " already in the game.");
        }
        console.log(players);
        console.log('SEPARATOR');
    }


    var reset = function(msg){

        numberOfWords = 0;
        wordEntered = 0;
        players = [];
        bot.sendMessage(msg.chat.id, "Resetting values...\nNumber of words: " + numberOfWords + "\nWords entered: " + wordEntered);
    }

    var newGame = function(msg){
        //Game table --> ID, date, numPlayers, wordsPerPlayer, %random, ended
        // CALL --> /newgame numPlayers,numTeams,wordsPerPlayer,%random
        //get parameters 
        //Total words = players x wordsPerPlayer


        //TODO -> CHACK IF NUMBERS
        var parameters = msg.text.split(' ')[1]
        var parameters2 = parameters.split(',');
        var numPlayers = parameters2[0];
        var numTeams = parameters2[1]
        var wordsPerPlayer = parameters2[2]
        var random_percentatge = parameters2[3]

        if(!random_percentatge.toString().endsWith('0')){
            random_percentatge = Math.round(random_percentatge)
        }

        if(isNaN(numPlayers) || isNaN(wordsPerPlayer) || isNaN(random_percentatge) || isNaN(numTeams)){
            bot.sendMessage(msg.chat.id, 'Some value is not a number... retry ')
        }else{
            var date = new Date()
            var values = [numPlayers, numTeams, wordsPerPlayer, random_percentatge, date, 0]; 

            const query = 'INSERT INTO game VALUES (NULL, ?, ?, ?, ?, ?, ?)'

            mysqlConnection.query(query, values, (err, rows, fields) => {
                if(err){
                    console.log(err)
                }else{
                    
                    
                    console.log(rows.insertId);
                    
            
                    currentGame.numPlayers = numPlayers
                    currentGame.numTeams = numTeams;
                    currentGame.wordsPerPlayer = wordsPerPlayer;
                    currentGame.random_percentatge = random_percentatge;
                    currentGame.wordsToInput = Math.round(((numPlayers * wordsPerPlayer) * random_percentatge)/100);
                    currentGame.WordsPerPlayerToInput = Math.round(currentGame.wordsToInput / currentGame.numPlayers);
                    bot.sendMessage(msg.chat.id, 'NEW GAME CREATED!\n\nGAME CONFIG: \nNumber of players: ' + numPlayers + '\nNumber of teams: ' + currentGame.numTeams + '\nWords per player: ' + wordsPerPlayer  + ' \nRandom words (%): ' + random_percentatge + '\nWord to be entered: ' +currentGame.wordsToInput+'\nWords to input per player: ' + currentGame.WordsPerPlayerToInput+ '\n')
                    bot.sendMessage(msg.chat.id, "/startgame to start!");
                    console.log(currentGame)
                }
            })
            //Insert new game in DB
        }

        
    }


    var help = function(msg){
        var string = "LIST OF COMMANDS: \n"
        commands.forEach(item => {
            string += '\n' + item.name + '  -  ' + item.desc
        })

        bot.sendMessage(msg.chat.id, string)
    }
}

