const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/', (req, res) => {
    mysqlConnection.query('SELECT * FROM employees', (err, rows, fields) =>{
        if(!err){
            res.json(rows);
        }else{
            console.log(err);
        }
    })
})


router.get('/:id', (req, res) => {
    const {id} = req.params;
    mysqlConnection.query('SELECT * FROM employees WHERE id = ?', [id], (err, rows, fields) => {
        if(!err){
            res.json(rows[0]);
        }else{
            console.log(err);
        }
    })
})


router.post('/', (req, res) =>{
    const {name, salary} = req.body;
    values = [name, salary];
    const query = `
       INSERT INTO employees VALUES (NULL, ? , ?);
    `; 
    mysqlConnection.query(query, values, (err, rows, fields) => {
        if(!err){
            res.json({Status: 'Employed saved'})
        }else{
            console.log(err)
        }
    })
})


router.put('/:id', (req, res) => {
    const {name, salary} = req.body;
    const id = req.params.id;
    values = [name, salary, id];
    
    const query = "UPDATE employees SET name = ?, salary = ? WHERE id = ?"; 

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if(err){
            res.json(err); 
            return; 
        }else{
            res.json({Status: 'Employed updated'});
        }
    })
})






module.exports = router;

