const env = {
    AWS_ACCESS_KEY: process.env.AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
    REGION : process.env.S3_REGION,
    Bucket: process.env.S3_BUCKET_NAME
  };
   
  module.exports = env;
  
